<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'text' => $faker->realText(),
        'post_id' => Post::inRandomOrder()->take(1)->pluck('id')->first(),
        'author_id' => User::inRandomOrder()->take(1)->pluck('id')->first(),
    ];
});