<?php

namespace Tests\Unit\Media;

use App\Models\Post;
use App\Services\MediaService;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Optix\Media\MediaUploader;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ServiceTest extends TestCase
{
    private $post;

    protected function setUp(): void
    {
        parent::setUp();

        $this->post = factory(Post::class)->create();
    }

    public function testUploadFile()
    {
        $files = [
            UploadedFile::fake()->image('file-name-1.jpg'),
            UploadedFile::fake()->image('file-name-2.jpg'),
            UploadedFile::fake()->image('file-name-3.jpg')
        ];

        $mediaService = new MediaService();
        $media = $mediaService->uploadFiles($files);

        $this->assertTrue(
            3 === $media->count()
        );
    }

    public function testNoFiles()
    {
        $files = [
            UploadedFile::fake()->image('file-name-1.jpg'),
            UploadedFile::fake()->image('file-name-2.jpg'),
            UploadedFile::fake()->image('file-name-3.jpg')
        ];

        $mediaService = new MediaService();

        $media = $mediaService->uploadFiles($files);

        $mediaService2 = new MediaService();

        $media2 = $mediaService2->media->merge($media);

        $this->assertEquals(
            $media,
            $media2
        );
    }

    public function testSyncModel()
    {
        $first = [
            UploadedFile::fake()->image('file-name-1.jpg'),
            UploadedFile::fake()->image('file-name-2.jpg'),
            UploadedFile::fake()->image('file-name-3.jpg')
        ];

        $mediaService = new MediaService();

        $media = $mediaService->uploadFiles($first);

        $this->post->attachMedia(
            $media->pluck('id')->toArray()
        );

        // загрузка файлов
        $this->assertTrue(
            3 === $this->post->media()->count()
        );

        $second = [
            UploadedFile::fake()->image('file-name-4.jpg'),
            UploadedFile::fake()->image('file-name-5.jpg'),
            UploadedFile::fake()->image('file-name-6.jpg')
        ];

        $mediaService = new MediaService();

        $media = $mediaService->uploadFiles($second);

        $this->post->attachMedia(
            $media->pluck('id')->toArray()
        );

        // добавление файлов
        $this->assertTrue(
            6 === $this->post->media()->count()
        );

        $third = [
            UploadedFile::fake()->image('file-name-7.jpg'),
            UploadedFile::fake()->image('file-name-8.jpg'),
            UploadedFile::fake()->image('file-name-9.jpg')
        ];

        $mediaService = new MediaService();

        $media = $mediaService->uploadFiles($third);

        $this->post->detachMedia();

        $this->post->attachMedia(
            $media->pluck('id')->toArray()
        );

        // переназначение файлов
        $this->assertTrue(
            3 === $this->post->media()->count()
        );
    }
}
