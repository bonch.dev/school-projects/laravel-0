<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Support\Carbon;
use InvalidArgumentException;

class CheckUserRegisterDays
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, int $registerDays)
    {
        if (!auth()->check()) {
            return response()->json([
                'message' => 'Вы не авторизованы',
            ], 401);
        }

        /** @var User */
        $authUser = auth()->user();

        $userCreatedDate = new Carbon($authUser->created_at);
        $differenceDays = $registerDays - $userCreatedDate->diff(now())->days;

        if ($differenceDays > 0) {
            return response()->json([
                'message' => "Вы зарегистрированы менее чем 7 дней назад. Вам осталось $differenceDays дней",
            ], 403);
        }

        return $next($request);
    }
}