<?php


namespace App\Services;


use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Optix\Media\MediaUploader;
use Optix\Media\Models\Media;

class MediaService
{
    public $media;
    public $files;

    /**
     * @param UploadedFile $file
     * @return Media
     */
    public function uploadFile(UploadedFile $file): Media
    {
        $media = MediaUploader::fromFile($file)->upload();

        return $media;
    }

    /**
     * @param array $files
     * @return Collection
     */
    public function uploadFiles(array $files): Collection
    {
        $this->files = $files;
        $media = [];

        foreach ($files as $file) {
            $media[] = $this->uploadFile($file);
        }

        $this->media = (new Collection())->merge($media);

        return $this->media;
    }
}
