<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\CreateRequest;
use App\Http\Requests\Post\UpdateRequest;
use App\Http\Resources\CommentResource;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * PostController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except(['show', 'index', 'comments']);
        $this->middleware('can:update,post')->only(['update']);
        $this->middleware('can:delete,post')->only(['destroy']);
        $this->middleware('user.register_days:7')->only(['store']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return PostResource::collection(
            Post::paginate(5)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $validated = $request->validated();

        $authUser = auth()->user();

        $post = $authUser->posts()->create(
            $validated
        );

        if (isset($validated['categories'])) {
            $post->categories()->sync(
                $validated['categories']
            );
        }

        if (isset($validated['media'])) {
            $post->detachMedia();
            $post->attachMedia(
                $validated['media'],
                'post'
            );
        }

        return response()->json([
            'message' => 'Запись была создана',
            'post' => new PostResource(
                $post
            )
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return PostResource
     */
    public function show(Post $post)
    {
        return new PostResource(
            $post
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Post $post)
    {
        $validated = $request->validated();

        $post->update(
            $validated
        );

        if (isset($validated['categories'])) {
            $post->categories()->sync(
                $validated['categories']
            );
        }

        if (isset($validated['media'])) {
            $post->detachMedia();
            $post->attachMedia(
                $validated['media'],
                'post'
            );
        }

        return response()->json([
            'message' => 'Запись была обновлена',
            'post' => new PostResource(
                $post
            )
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return response()->json([
            'message' => 'Запись была удалена'
        ], 201);
    }

    /**
     * @param Request $request
     * @param Post $post
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function comments(Request $request, Post $post)
    {
        return CommentResource::collection(
            $post->comments()->paginate(5)
        );
    }
}