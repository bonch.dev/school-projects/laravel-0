<?php

namespace App\Http\Controllers;

use App\Http\Requests\Media\StoreRequest;
use App\Http\Requests\Media\UpdateRequest;
use Illuminate\Http\Request;
use Optix\Media\MediaUploader;
use Optix\Media\Models\Media;

class FileController extends Controller
{
    private $mediaService;

    /**
     * MediaController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api')
            ->only(['store', 'update', 'destroy']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json([
            Media::all()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $validated = $request->validated();

        $media = MediaUploader::fromFile($validated['file'])
            ->upload();

        return response()->json([
            'message' => 'Файл загружен',
            'media' => $media
        ], 201);
    }

    /**
     * @param Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Media $file)
    {
        return response()->json([
            'media' => $file
        ]);
    }

    /**
     * @param Request $request
     * @param Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Media $file)
    {
        $validated = $request->validated();

        $media->update(
            $validated
        );

        return response()->json([
            'message' => 'Файл обновлен',
            'media' => $media
        ], 201);
    }

    /**
     * @param Media $media
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Media $file)
    {
        $media->delete();

        return response()->json([
            'message' => 'Файл был удален',
            'media' => $media
        ], 201);
    }
}
