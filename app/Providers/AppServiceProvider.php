<?php

namespace App\Providers;

use App\Models\Post;
use App\Observers\PostObserver;
use Illuminate\Support\ServiceProvider;
use Intervention\Image\Image;
use Optix\Media\Facades\Conversion;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (config("app.is_secure")) {
            \URL::forceScheme('https');
        }

        Post::observe(PostObserver::class);

        Conversion::register('thumbnail', function (Image $image) {
            return $image->fit(200, 200);
        });
    }
}