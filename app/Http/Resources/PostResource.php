<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => Str::title($this->title),
            'slug' => $this->slug,
            'content' => $this->content,
            'created_at' => Carbon::parse($this->created_at)->toDayDateTimeString(),
            'author' => new UserResource($this->author),
            'categories' => CategoryResource::collection($this->categories),
            'media' => $this->media
        ];
    }
}
