<?php

namespace Tests;

use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Carbon;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;

    protected $faker;
    protected $user;
    protected $anotherUser;

    protected function setUp(): void
    {
        parent::setUp();

        $this->artisan('migrate');

        $this->user = factory(User::class)->create([
            'created_at' => Carbon::now()->subDays(10)
        ]);

        $this->anotherUser = factory(User::class)->create([
            'created_at' => Carbon::now()->subDays(10)
        ]);

        $this->faker = Faker::create();
    }
}
