<?php

namespace Tests\Unit\Middleware;

use App\Http\Middleware\CheckUserRegisterDays;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CheckUserRegisterDaysTest extends TestCase
{
    private $zoomer;
    private $boomer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->zoomer = factory(User::class)->create();
        $this->boomer = factory(User::class)->create([
            'created_at' => Carbon::now()->subDays(30)
        ]);
    }

    /**
     * Успешное выполнение
     */
    public function testSuccess()
    {
        $this->actingAs($this->boomer, 'api');

        $middleware = new CheckUserRegisterDays();

        $request = Request::create(
            route('posts.store'),
            'POST'
        );

        $response = $middleware->handle(
            $request,
            function () { },
            7
        );

        $this->assertEquals($response, null);
    }

    /**
     * Пользователь не проходит проверку
     */
    public function testFail()
    {
        $this->actingAs($this->zoomer, 'api');

        $middleware = new CheckUserRegisterDays();

        $request = Request::create(
            route('posts.store'),
            'POST'
        );

        $response = $middleware->handle(
            $request,
            function () { },
            7
        );

        $this->assertEquals($response->getStatusCode(), 403);
    }

    /**
     * Не пользователь
     */
    public function testNoAuth()
    {
        $middleware = new CheckUserRegisterDays();
        $request = Request::create(
            route('posts.store'),
            'POST'
        );

        $response = $middleware->handle(
            $request,
            function () { },
            7
        );

        $this->assertEquals($response->getStatusCode(), 401);
        $this->assertEquals(
            $response->getData()->message,
            'Вы не авторизованы'
        );
    }
}