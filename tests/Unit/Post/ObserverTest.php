<?php

namespace Tests\Unit\Post;

use App\Models\Post;
use Illuminate\Support\Str;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ObserverTest extends TestCase
{
    public function testSlugOnCreating()
    {
        $post = factory(Post::class)->create();

        $this->assertTrue(
            $post->slug != null
        );
    }

    public function testSlugEqualsTitle()
    {
        $post = factory(Post::class)->create();

        $this->assertTrue(
            $post->slug == Str::slug($post->title)
        );
    }
}
