<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
    Route::post('login', 'Auth\LoginController@login')->name('login');
    Route::post('register', 'Auth\RegisterController@create')->name('register');
    Route::middleware('auth:api')->get('logout', 'Auth\LoginController@logout')->name('logout');
});

Route::get('posts/{post}/comments', 'PostController@comments')->name('posts.comments');
Route::apiResource('posts', 'PostController');
Route::apiResource('categories', 'CategoryController');
Route::apiResource('comments', 'CommentController');
Route::apiResource('files', 'FileController');
