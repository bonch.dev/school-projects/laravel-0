<?php

namespace Tests\Feature\Post;

use App\Models\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateTest extends TestCase
{
    private $post;

    protected function setUp(): void
    {
        parent::setUp();

        $this->post = $this->user->posts()
            ->save(factory(Post::class)->make());
    }
    /**
     * Успешное обновление
     */
    public function testSuccess()
    {
        $data = [
            'title' => 'Новый заголовок'
        ];

        $response = $this
            ->actingAs($this->user, 'api')
            ->putJson(
                route('posts.update', $this->post),
                $data
            );

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'message', 'post'
            ]);
    }

    /**
     * Некорректные данные
     */
    public function testInvalidData()
    {
        $data = [
            'title' => false
        ];

        $response = $this
            ->actingAs($this->user, 'api')
            ->putJson(
                route('posts.update', $this->post),
                $data
            );

        $response
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors'
            ]);
    }

    /**
     * Без пользователя
     */
    public function testNoAuth()
    {
        $data = [
            'title' => 'Новый заголовок'
        ];

        $response = $this
            ->putJson(
                route('posts.update', $this->post),
                $data
            );

        $response
            ->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    /**
     * Другой пользователь
     */
    public function testAnotherUser()
    {
        $data = [
            'title' => 'Новый заголовок'
        ];

        $response = $this
            ->actingAs($this->anotherUser, 'api')
            ->putJson(
                route('posts.update', $this->post),
                $data
            );

        $response
            ->assertStatus(403)
            ->assertJsonStructure([
                'message'
            ]);
    }
}