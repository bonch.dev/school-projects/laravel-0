<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function guard()
    {
        return Auth::guard('api');
    }

    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (Auth::attempt($credentials)) {
            $token = Str::random(60);

            $authUser = auth()->user();

            $authUser->forceFill([
                'api_token' => hash('sha256', $token)
            ])->save();

            return response()->json([
                'message' => 'Вы вошли в систему',
                'user' => $authUser,
                'token' => $token
            ]);
        }

        return response()->json([
            'message' => 'Не удалось войти в систему'
        ], 422);
    }

    public function logout()
    {
        $authUser = auth()->user();

        $authUser->forceFill([
            'api_token' => null
        ])->save();

        return response()->json([
            'message' => 'Вы вышли из системы'
        ]);
    }
}