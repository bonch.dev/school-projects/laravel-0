<?php

namespace Tests\Feature\Post;

use App\Models\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DestroyTest extends TestCase
{
    private $post;

    protected function setUp(): void
    {
        parent::setUp();

        $this->post = $this->user->posts()
            ->save(factory(Post::class)->make());
    }

    /**
     * Успешное удаление
     */
    public function testSuccess()
    {
        $response = $this
            ->actingAs($this->user, 'api')
            ->deleteJson(
                route('posts.destroy', $this->post)
            );

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'message'
            ]);
    }

    /**
     * Удаление другим пользователем
     */
    public function testAnotherUser()
    {
        $response = $this
            ->actingAs($this->anotherUser, 'api')
            ->deleteJson(
                route('posts.destroy', $this->post)
            );

        $response
            ->assertStatus(403)
            ->assertJsonStructure([
                'message'
            ]);
    }

    /**
     * Удаление без пользователя
     */
    public function testNoAuth()
    {
        $response = $this
            ->deleteJson(
                route('posts.destroy', $this->post)
            );

        $response
            ->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }
}