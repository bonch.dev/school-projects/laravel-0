<?php

namespace Tests\Feature\Media;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Optix\Media\MediaUploader;
use Optix\Media\Models\Media;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreTest extends TestCase
{
    public function testSuccessShow()
    {
        $media = MediaUploader::fromFile(
            UploadedFile::fake()->image('file-name-1.jpg')
        )->upload();

        $response = $this->actingAs($this->user, 'api')
            ->getJson(
                route('files.show', $media)
            );

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'media'
            ]);
    }

    public function testSuccess()
    {
        $data = [
            'file' => UploadedFile::fake()->image('file-name-1.jpg'),
            // 'file' => UploadedFile::fake()->create('file-name-1.exe')
        ];

        $response = $this->actingAs($this->user, 'api')
            ->post(
                route('files.store'),
                $data,
                [
                    'Accept' => 'application/json'
                ]
            );

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'media', 'message'
            ]);
    }

    public function testInvalidFile()
    {
        $data = [
            'file' => UploadedFile::fake()->create('script.php')
        ];

        $response = $this->actingAs($this->user, 'api')
            ->post(
                route('files.store'),
                $data,
                [
                    'Accept' => 'application/json'
                ]
            );

        $response
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors'
            ]);
    }

    public function testExistsFile()
    {
        $file = UploadedFile::fake()->image('file-name.jpg');

        $media = MediaUploader::fromFile($file)->upload();

        $response = $this->actingAs($this->user, 'api')
            ->delete(
                route('files.destroy', $media),
                [
                    'Accept' => 'application/json'
                ]
            );

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'message'
            ]);

        $this->assertTrue($media->filesystem()->exists($media->getPath()));
    }
}
