<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Post;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->words(random_int(2, 10), true),
        'slug' => null,
        'content' => $faker->realText(),
        'author_id' => User::inRandomOrder()->take(1)->pluck('id')->first(),
    ];
});