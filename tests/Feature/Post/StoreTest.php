<?php

namespace Tests\Feature\Post;

use App\Models\Category;
use App\Models\User;
use Illuminate\Support\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Успешное добавление записи
     */
    public function testSuccess()
    {
        $data = [
            'title' => $this->faker->words(random_int(2, 7), true),
            'content' => $this->faker->realText(),
        ];

        $response = $this
            ->actingAs($this->user, 'api')
            ->postJson(
                route('posts.store'),
                $data
            );

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'message', 'post'
            ]);
    }

    /**
     * Некорректные данные
     */
    public function testInvalidData()
    {
        $data = [
            'title' => null,
            'content' => false,
        ];

        $response = $this
            ->actingAs($this->user, 'api')
            ->postJson(
                route('posts.store'),
                $data
            );

        $response
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors'
            ]);
    }

    /**
     * Незарегистрированный пользователь
     */
    public function testNoAuth()
    {
        $data = [
            'title' => $this->faker->words(random_int(2, 7), true),
            'content' => $this->faker->realText(),
        ];

        $response = $this
            ->postJson(
                route('posts.store'),
                $data
            );

        $response
            ->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    /**
     * Проверка добавления категорий
     */
    public function testCategories()
    {
        $categories = factory(Category::class, 2)->create()
            ->pluck('id')->toArray();

        $data = [
            'title' => $this->faker->words(random_int(2, 7), true),
            'content' => $this->faker->realText(),
            'categories' => $categories
        ];

        $response = $this
            ->actingAs($this->user, 'api')
            ->postJson(
                route('posts.store'),
                $data
            );

        $response
            ->dump()
            ->assertStatus(201)
            ->assertJsonStructure([
                'message', 'post'
            ]);
    }

    /**
     * Проверка некорректных категорий
     */
    public function testInvalidCategories()
    {
        $data = [
            'title' => $this->faker->words(random_int(2, 7), true),
            'content' => $this->faker->realText(),
            'categories' => ['lorem', 'ipsum', 'dolor']
        ];

        $response = $this
            ->actingAs($this->user, 'api')
            ->postJson(
                route('posts.store'),
                $data
            );

        $response
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors'
            ]);
    }
}